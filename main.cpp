#include <iostream>
#include "cesar.h"
#include "vigenere.h"

using namespace std;
using namespace cesarLib;
using namespace vigenereLib;

/**
 * \brief Programme de tests.
 * \author Karoun Ali
 * \version 1.0
 * \date 8 octobre 2017
 *
 * Programme de test pour les classe cesar et vigenere
 *
 */

int main(){

//    /************************1 bloc : cesar******************/

    cout << "*********************************"<<endl;
    cout << "******* Chiffrement Cesar *******" << endl;
    cout << "*********************************"<<endl;
    int key,choix;
    string texte{"J AVAIS COMMENCE LA PUBLICATION DES POESIES DE PEIRE RAIMON"
                 "DE TOULOUSE DANS LA REVUE L AUTA ORGANE DE LA SOCIETE DES"
                 "TOULOUSAINS DE TOULOUSE MALGRE LA BONNE VOLONTE DE LA SOCIETE"
                 "ET DE SON PRESIDENT LES CIRCONSTANCES NE SE PRETERENT PAS A LA"
                 "CONTINUATION DE CE TRAVAIL JE L'ARRETAI DONC APRES AVOIR PUBLIE"
                 "QUATRE PIECES CETTE EDITION ETAIT DESTINEE A DES LECTEURS NON"
                 "INITIES EN GENERAL A LA PHILOLOGIE ROMANE MAIS CONNAISSANT LEUR"
                 "LANGUE MATERNELLE IL NOUS FAUDRAIT BIEN DECIDER EN ATTENDANT DES"
                 "EDITIONS CRITIQUES QUI NE PARAISSENT QU'A DE LONGS INTERVALLES ET QUI"
                 "NE PARAISSENT PAS TOUTES EN FRANCE A AVOIR DES EDITIONS PROVISOIRES"
                 "DE NOS TROUBADOURS DONT LE TEXTE SERAIT EMPRUNTE A QUELQUES BONS"
                 "MANUSCRITS NOS TROUBADOURS ET JE DIS NOS A DESSEIN NE"
                 "SONT PAS FAITS EXCLUSIVEMENT POUR SERVIR DE THEME A DES EXERCICES"
                 "PHILOLOGIQUES CE SONT DES POETES FACILEMENT ABORDABLES, ET DONT LA"
                 "POESIE N EST PAS TOUT A FAIT ETEINTE MALGRE LES ANS NOUS NE SAVONS"
                 "QUAND TOUS NOS TROUBADOURS MEME QUELQUES-UNS DES PLUS GRANDS SERONT"
                 "EDITES D UNE MANIERE CRITIQUE FAUT IL SE RESIGNER JUSQUE LA A"
                 "LES LIRE DANS LES RECUEILS INTROUVABLES ET D'UN SI JOLI"
                 "ASPECT TYPOGRAPHIQUE! DE MAHN OU DANS LE RECUEIL PLUS BEAU"
                 "TYPOGRAPHIQUEMENT MAIS AUSSI RARE DE RAYNOUARD NOUS NE LE CROYONS"};

    cout << endl;
    cesar ces(texte);
    cout << "Voici le texte a chiffrer : " << endl;
    cout << endl;
    cout <<  ces.getTxt() << endl;
    cout <<"Entrez la cle desirer : ";
    cin >> key ;
    ces.chiffrement(key);
    cout << endl;
    cout << "Dechiffrer avec (1) ou sans cle (2)";
    cin >> choix ;

    while (choix != 1 && choix != 2) {
        cout << "Dechiffrer avec (1) ou sans cle (2)";
        cin >> choix;
    }
    switch (choix) {
    case 1:
        ces.dechiffrement_Key(key);
        break;
    case 2:
        ces.dechiffrement_No_Key();
        break;
    default:
        break;
    }
    cout << endl;
    cout << "Texte chiffrer : " << endl;
    cout << endl;
    cout <<ces.getTxtCoder()<<endl;
    cout << endl;
    cout << "Texte dechiffrer : " <<endl;
    cout << endl;
    cout << ces.getTxtDecoder()<<endl;

    /**************************************2ieme bloc : vigenere******************/

    //    cout << "*********************************"<<endl;
    //    cout << "******* Chiffrement Vigenere *******" << endl;
    //    cout << "*********************************"<<endl;
    //    string texte {"J AVAIS COMMENCE LA PUBLICATION DES POESIES DE PEIRE RAIMON"
    //                  "DE TOULOUSE DANS LA REVUE L AUTA ORGANE DE LA SOCIETE DES"
    //                  "TOULOUSAINS DE TOULOUSE MALGRE LA BONNE VOLONTE DE LA SOCIETE"
    //                  "ET DE SON PRESIDENT LES CIRCONSTANCES NE SE PRETERENT PAS A LA"
    //                  "CONTINUATION DE CE TRAVAIL JE L'ARRETAI DONC APRES AVOIR PUBLIE"
    //                  "QUATRE PIECES CETTE EDITION ETAIT DESTINEE A DES LECTEURS NON"
    //                  "INITIES EN GENERAL A LA PHILOLOGIE ROMANE MAIS CONNAISSANT LEUR"
    //                  "LANGUE MATERNELLE IL NOUS FAUDRAIT BIEN DECIDER EN ATTENDANT DES"
    //                  "EDITIONS CRITIQUES QUI NE PARAISSENT QU'A DE LONGS INTERVALLES ET QUI"
    //                  "NE PARAISSENT PAS TOUTES EN FRANCE A AVOIR DES EDITIONS PROVISOIRES"
    //                  "DE NOS TROUBADOURS DONT LE TEXTE SERAIT EMPRUNTE A QUELQUES BONS"
    //                  "MANUSCRITS NOS TROUBADOURS ET JE DIS NOS A DESSEIN NE"
    //                  "SONT PAS FAITS EXCLUSIVEMENT POUR SERVIR DE THEME A DES EXERCICES"
    //                  "PHILOLOGIQUES CE SONT DES POETES FACILEMENT ABORDABLES, ET DONT LA"
    //                  "POESIE N EST PAS TOUT A FAIT ETEINTE MALGRE LES ANS NOUS NE SAVONS"
    //                  "QUAND TOUS NOS TROUBADOURS MEME QUELQUES-UNS DES PLUS GRANDS SERONT"
    //                  "EDITES D UNE MANIERE CRITIQUE FAUT IL SE RESIGNER JUSQUE LA A"
    //                  "LES LIRE DANS LES RECUEILS INTROUVABLES ET D'UN SI JOLI"
    //                  "ASPECT TYPOGRAPHIQUE! DE MAHN OU DANS LE RECUEIL PLUS BEAU"
    //                  "TYPOGRAPHIQUEMENT MAIS AUSSI RARE DE RAYNOUARD NOUS NE LE CROYONS"
    //                 };

    //    unsigned choix;
    //    string key;
    //    vigenere vige(texte);
    //    cout << "Voici le texte a chiffrer : " << endl;
    //    cout << endl;
    //    cout << vige.getTxt() << endl;
    //    cout << "Entre un texte correspondant a la cle que vous voulez : ";
    //    cin >> key;

    //    vige.keyTxt(key);
    //    vige.chiffrement();

    //    cout << endl;
    //    cout << "Dechiffrer avec (1) ou sans cle (2) : ";
    //    cin >> choix ;

    //    while (choix != 1 && choix != 2) {
    //        cout << "Dechiffrer avec (1) ou sans cle (2) : ";
    //        cin >> choix;
    //    }
    //    switch (choix) {
    //    case 1:
    //        vige.dechiffrementKey();
    //        break;
    //    case 2:
    //        vige.dechiffrementNoKey();
    //        break;
    //    default:
    //        break;
    //    }

    //    cout << endl;
    //    cout << "Texte chiffrer : " << endl;
    //    cout << endl;
    //    cout <<vige.getTxtCoder()<<endl;
    //    cout << endl;
    //    cout << "Texte cle : " <<endl;
    //    cout << vige.getKey()<<endl;
    //    cout << "Texte dechiffrer : " <<endl;
    //    cout << endl;
    //    cout << vige.getTxtDecoder()<<endl;



    return 0;

}
