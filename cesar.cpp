#include "cesar.h"

namespace cesarLib {

cesar::cesar(string &message) {
    txt_=message;
    uniforme();
    txt_.erase(remove(txt_.begin(), txt_.end(), ' '),txt_.end()); //supprime les esapces blanc
    transform(txt_.begin(), txt_.end(), txt_.begin(), ::tolower); // changer les majuscule d'une chaine en minuscule
}

void cesar::uniforme(){ //supprime tout les accants

    string accent("ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÌÍÎÏìíîïÙÚÛÜùúûüÿÑñÇç");
    string sansAccent("AAAAAAaaaaaaOOOOOOooooooEEEEeeeeIIIIiiiiUUUUuuuuyNnCc");

    unsigned i=0,j=0,k=0,taille;

    taille=txt_.size();

    for (i=0;i<=txt_.size();i++)
    {
        for(j=0;j<=104;j++)
        {
            if((txt_[i]==accent[j])&&(txt_[i+1]==accent[j+1]))
            {
                txt_[i]=sansAccent[j/2];
                for(k=i+1;k<taille;k++)
                {
                    txt_[k]=txt_[k+1];
                }
                txt_=txt_.substr(0,taille-1);
                taille=txt_.size();
            }
        }
    }
}

void cesar::chiffrement(int theKey){

    for (unsigned i = 0; i < txt_.size(); ++i) {
        char c = txt_[i];
        for (int j = 0; j < 26; ++j) {
            if(c == alpha[j]){
                txtCoder += alpha[(j + theKey)%26];
            }
        }
    }
}


void cesar::dechiffrement_Key(int theKey){

    for (unsigned i = 0; i < txtCoder.size(); ++i) {
        char c = txtCoder[i];
        for (unsigned j = 0; j < 26; ++j) {
            if(c == alpha[j]){
                int test = j - theKey;
                (test < 0) ?  txtDecoder += alpha[(j - theKey)+26]
                               :  txtDecoder += alpha[(j - theKey)];
            }
        }
    }
}

bool pred(const std::pair<char, int>& lhs,const std::pair<char, int>& rhs)
{
    return lhs.second < rhs.second;
}

void cesar::dechiffrement_No_Key(){

    char lettre_Frequente1 {'e'};
    //char lettre_Frequente2;
    map<char,int>analyse;
    map<char,int>::iterator it;

    for (char c= 'a'; c <='z'; ++c) {       // cree une map avec les lettre de l'alphabet
        analyse[c] = 0;
    }

    for (unsigned i = 0; i < txtCoder.size(); ++i) {    //analyse la frequence de chaque lettre dans le txtCoder
        char c = txtCoder[i];
        analyse[c]++;
    }

    it = max_element(analyse.begin(), analyse.end(), pred);    //retourne l'element le plus fréquent

    int key = it->first - lettre_Frequente1;   // soustrait la lettre le plus frequente du texte coder avec le lettre la plus frequente de laphabet

    dechiffrement_Key(key);
}

}
