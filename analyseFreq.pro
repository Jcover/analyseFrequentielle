TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt


SOURCES += main.cpp \
    cesar.cpp \
    vigenere.cpp

HEADERS += \
    cesar.h \
    vigenere.h
