#include "vigenere.h"


namespace vigenereLib{


vigenere::vigenere(string &message){
    txt = message;
    uniforme(); //supprime tout les accants
    txt.erase(remove(txt.begin(), txt.end(), ' '),txt.end()); //supprime les esapces blanc
    transform(txt.begin(), txt.end(), txt.begin(), ::tolower); // transforme la chaine en miniscule
    //creat_tab_vigenere(); bad alloc ! trop d'espace allouer lors de la construction du tableau dans l'objet ??.
}

void vigenere::creatTabVigenere()
{
    unsigned i,j;
    char a {97};

    for (i=0; i<=26; i++)
    {
        for (j=0; j<=26; j++, a++)
        {
            if (i==0 && j==0)
            {
                tab_vig[i][j]= a;
            }
            else if (a == 123)
            {
                a = 97;
                tab_vig[i][j]= a;
            }
            else
            {
                tab_vig[i][j] = a;
            }
        }
    }
}

void vigenere::uniforme(){

    string accent("ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÌÍÎÏìíîïÙÚÛÜùúûüÿÑñÇç");
    string sansAccent("AAAAAAaaaaaaOOOOOOooooooEEEEeeeeIIIIiiiiUUUUuuuuyNnCc");

    unsigned i=0,j=0,k=0,taille;

    taille=txt.size();

    for (i=0;i<=txt.size();i++)
    {
        for(j=0;j<=104;j++)
        {
            if((txt[i]==accent[j])&&(txt[i+1]==accent[j+1]))
            {
                txt[i]=sansAccent[j/2];
                for(k=i+1;k<taille;k++)
                {
                    txt[k]=txt[k+1];
                }
                txt=txt.substr(0,taille-1);
                taille=txt.size();
            }
        }
    }
}

void vigenere::keyTxt(string &theKey){

    unsigned j{0};

    for (unsigned i{0}; i < txt.size(); ++i) {
        if (j >= theKey.size()) {
            j = 0;
        }
        key += theKey.at(j);
        j++;
    }
}

unsigned vigenere::charToInt(char a){

    unsigned cpt{0};

    for (char c = 'a'; c <= 'z'; ++c) {
        if(c == a){
            return cpt;
        }
        cpt++;
    }
}

void vigenere::chiffrement(){

    unsigned ligne,colonne;

    creatTabVigenere();

    for (unsigned i = 0; i < key.size(); ++i) {
        ligne = charToInt( key.at(i));
        colonne = charToInt(txt.at(i));
        txtCoder += tab_vig[ligne][colonne];
    }

}

void vigenere::dechiffrementKey(){

    int lettreKey,lettreChiffre,lettreBrute;

    for (unsigned i = 0; i < key.size(); ++i) {

        lettreKey = key.at(i);
        lettreChiffre = txtCoder.at(i);

        if (lettreKey > lettreChiffre) {
            lettreBrute = (26 - lettreKey)+lettreChiffre;
            txtDecoder += tab_vig[lettreBrute][0];
        }else{
            lettreBrute=lettreChiffre-lettreKey;
            txtDecoder += tab_vig[lettreBrute][0];
        }
    }
}

bool pred(const std::pair<char, int>& lhs,const std::pair<char, int>& rhs)
{
    return lhs.second < rhs.second;
}

void vigenere::creatMap(map<char,int>& analyse)
{
    for (char c= 'a'; c <='z'; ++c) {       // cree une map avec les lettre de l'alphabet
        analyse[c] = 0;
    }
}

void vigenere::dechiffrementNoKey(){

    int tailleRepet = 3;
    unsigned cpt{1};
    int firstDistance,secondDistance;
    string repetition;
    string::size_type n;
    int tailleKey;
    list<string> listeSubStr;
    map<char,int>analyse;
    map<char,int>::iterator it;
    string dechiffre;

    creatMap(analyse);

    for (unsigned i = 0; i < txtCoder.size()-tailleRepet+1; ++i) {

        if(cpt ==3){
            break;
        }

        repetition = txtCoder.substr(i,tailleRepet);  //decoupe en sous chaine de taille n toute la chaine
        n = txtCoder.find(repetition,(i+tailleRepet)-1);    //cherche la repetion apres sa position

        if (n != string::npos && cpt <= 2) {    //npos +- comme EOF

            switch (cpt) {
            case 1:
                firstDistance = n - i ;      //distance entre la premiere repetition trouver et la repetition
                cpt++;
                break;
            case 2:
                secondDistance = n - i ;      //distance entre la premiere repetition trouver et la repetition
                cpt++;
                break;
            default:
                break;
            }
        }
    }

    tailleKey = txtCoder.size()/std::__gcd(firstDistance,secondDistance);

    //    for (unsigned i = 0; i < txtCoder.size()-tailleKey+1; i=i+tailleKey) {

    //        listeSubStr.push_back(txtCoder.substr(i,tailleKey));
    //    }

    //    for (int j = 0; j < tailleKey; ++j) {

    //        for( string s : listeSubStr) {
    //            analyse[s.at(j)]++;
    //        }
    //        it = max_element(analyse.begin(), analyse.end(), pred);
    //         char key = it->first - lettre_Frequente1;
    //        dechiffre += key;
    //        creatMap(analyse);
    //    }

    string cle{""};

    for (int var = 0; var < tailleKey; ++var) {
        string tmp{""};

        for (unsigned var2 = var; var2 < txt.size(); var2 = var2+tailleKey) {
            tmp+= txt.at(var2);
        }

        cesar ces(txt);
        ces.dechiffrement_No_Key();
        cle += ces.getTxtDecoder();
    }

}



}








