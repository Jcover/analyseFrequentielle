#ifndef VIGENERE_H
#define VIGENERE_H

#include <iostream>
#include <algorithm>
#include <cctype>
#include <map>
#include <utility>
#include <numeric>
#include <list>
#include "cesar.h"

using namespace std;
using namespace cesarLib;

/** @namespace cesarLib
 *
 * espace de nommage regroupant les outils pour
 * le chiffrement vigenere
 */
namespace vigenereLib{

/**
 * @class vigenere
 * @brief classe representant le chiffrement vigenere
 *
 *  La classe gere le chiffrement et le dechiffrement avec et sans cle
 */
class vigenere
{

    /**
     * @brief Texte brute
     */
    string txt;

    /**
     * @brief Texte chiffrer
     */
    string txtCoder;

    /**
     * @brief Texte decoder
     */
    string txtDecoder;

    /**
     * @brief Texte correspondant a la cle
     */
    string key;

    /**
      * @brief Tableau de vigenere
      *
      */
    char tab_vig[26][26];


public:

    /**
     *  @brief Constructeur
     *
     *  Constructeur de la classe vigenere
     *
     *  @param message : le message a chiffrer
     */
    vigenere(string &message);

    /**
     * @brief Methode qui recopie plusieur fois la cle juqu'a
     *  avoir la meme taille que le texte brute
     *
     * @param theKey : la clé
     */
    void keyTxt(string &theKey);

    /**
     * @brief Methode qui crée un tableau de vigenere
     *
     * Methode fortement inspiré de https://openclassrooms.com/forum/sujet/chiffre-de-vigenere-44573
     */
    void creatTabVigenere();

    /**
     *  @brief methode de chiffrement
     *
     *  Methode qui permet de chiffrer un texte avec la clé.
     * Cette clé est initialiser a l'aide de la methode keyTxt
     *
     *  @param theKey : la cle de chiffrement
     */
    void chiffrement();

    /**
     * @brief Methode de dechiffrement avec cle
     *
     * Methode qui permet de dechiffrer un texte avec la clé.
     * Cette clé est initialiser a l'aide de la methode keyTxt
     *
     * @param theKey : le cle de dechiffrement
     */
    void dechiffrementKey();

    /**
     * @brief Methode de dechiffrement sans cle
     *
     * Methode qui permet de dechiffrer un texte sans clé.
     * Cette cle est trouver par un systeme qui cherche les repetitions dans le texte
     * chiffrer
     */
    void dechiffrementNoKey();

    /**
     * @brief Accesseurs pour le texte brute
     *
     * @return retourne le texte brute
     */
    inline string getTxt();

    /**
     * @brief Accesseurs pour le texte chiffrer
     *
     * @return retourne le texte chiffrer
     */
    inline string getTxtCoder();

    /**
     * @brief Accesseurs pour le texte dechiffrer
     *
     * @return retourne le texte dechiffrer
     */
    inline string getTxtDecoder();

    /**
     * @brief Accesseurs pour la cle
     *
     * @return retourne la cle
     */
    inline string getKey();

private:

    /**
     * @brief  methode uniforme
     *
     *  Methode qui supprime les caractere special
     */
    void uniforme();

    /**
     * @brief methode qui convertis un char en int de l'alphabet
     * @param a : caractere a convertir
     */
    unsigned charToInt(char a);

    /**
     * @brief methode qui cree une map contenant le redondance des lettre de l'aphabet
     * @param analyse : map a initialisé
     */
    void creatMap(map<char, int> &analyse);
};

inline string vigenere::getTxt(){
    return txt;
}
inline string vigenere::getTxtCoder(){
    return txtCoder;
}

inline string vigenere::getTxtDecoder(){
    return txtDecoder;
}

inline string vigenere::getKey(){
    return key;
}


}

#endif // VIGENERE_H
