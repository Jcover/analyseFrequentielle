#ifndef CESAR_H
#define CESAR_H
#include <iostream>
#include <algorithm>
#include <cctype>
#include <map>
#include <utility>



using namespace std;

/** \namespace cesarLib
 *
 * espace de nommage regroupant les outils pour
 * le chiffrement cesar
 */

namespace cesarLib {

/**
 * @class cesar
 * @brief classe representant le chiffrement cesar
 *
 *  La classe gere le chiffrement et le dechiffrement avec et sans cle
 */
class cesar {

    /**
     * @brief Texte brute
     */
    string txt_;

    /**
     * @brief Texte chiffrer
     */
    string txtCoder;

    /**
     * @brief Texte decoder
     */
    string txtDecoder;

    /**
      * @brief Tableau d'alphabet
      *
      */
    char const alpha[26]{'a','b','c','d','e','f','g','h','i','j','k','l','m','n',
                         'o','p','q','r','s','t','u','v','w','x','y','z'};

public:
    /**
     *  @brief Constructeur
     *
     *  Constructeur de la classe cesar
     *
     *  @param message : le message a chiffrer
     */
    cesar(string &message);

    /**
     *  @brief methode de chiffrement
     *
     *  Methode qui permet de chiffrer un texte avec la clé passé en param
     *
     *  @param theKey : la cle de chiffrement
     */
    void chiffrement(int theKey);

    /**
     * \brief methode de dechiffrement avec cle
     *
     * Methode qui permet de dechiffrer un texte avec la clé passé en param
     *
     * @param theKey : le cle de dechiffrement
     */
    void dechiffrement_Key(int theKey);

    /**
     * @brief methode de dechiffrement sans cle
     *
     * Methode qui permet de dechiffrer un texte sans clé passé en param
     */
    void dechiffrement_No_Key();

    /**
     * @brief Accesseurs pour le texte brute
     *
     * @return retourne le texte brute
     */
    inline string getTxt();

    /**
     * @brief Accesseurs pour le texte chiffrer
     *
     * @return retourne le texte brute
     */
    inline string getTxtCoder();

    /**
     * @brief Accesseurs pour le texte dechiffrer
     *
     * @return retourne le texte brute
     */
    inline string getTxtDecoder();

private:

    /**
     * @brief  methode uniforme
     *
     *  Methode qui supprime les caractere special
     */
    void uniforme();

};

inline string cesar::getTxt(){
    return txt_;
}
inline string cesar::getTxtCoder(){
    return txtCoder;
}

inline string cesar::getTxtDecoder(){
    return txtDecoder;
}

}

#endif // CESAR_H
